cordova.define("kapsel-plugin-fioriclient.FioriClient-Launchpad", function(require, exports, module) {
var cc = function(msg) {
    console.log("[FioriClient][launchpad.js] " + msg);
};
var isToolbarSupported = function() {
    return (typeof sap !== "undefined" && typeof sap.Toolbar !== "undefined");
};
var isUsageSupported = function() {
    return (typeof sap !== "undefined" && typeof sap.Usage !== "undefined");
};

var fioriURL;
var tileObserver;

var launchpadTimer = function() {

    var startLoadTimer = function startLoadTimer(timeout, successCallback, errorCallback) {
        if (typeof successCallback !== "function") successCallback = function() {
            cc("startLoadTimer success");
        };
        if (typeof errorCallback !== "function") errorCallback = function(error) {
            cc("startLoadTimer error: " + JSON.stringify(error));
        };

        if (typeof timeout !== "undefined" && (typeof timeout !== "number" || timeout < 0)) {
            cc("If timeout is specified, it must be a number >= 0. current is: " + typeof timeout);
            errorCallback("invalid_argument", "Timeout must be undefined or a number");
            return;
        }

        if (isUsageSupported()) {
            var infoObj = {};
            if (typeof timeout === "number") {
                infoObj.timeout = timeout;
            }
            cordova.exec(successCallback, errorCallback, "FioriClient", "startLaunchpadLoadTimer", [infoObj]);
        } else {
            errorCallback("usage_not_supported", "Usage is not supported.");
        }
    };

    var stopLoadTimer = function stopLoadTimer(tileCount, infoType, successCallback, errorCallback) {
        if (typeof successCallback !== "function") successCallback = function() {
            cc("stopLoadTimer success");
        };
        if (typeof errorCallback !== "function") errorCallback = function(error) {
            cc("stopLoadTimer error: " + JSON.stringify(error));
        };

        if (typeof tileCount !== "undefined" && (typeof tileCount !== "number" || tileCount < 0)) {
            errorCallback("if tileCount specified, it must be a number >= 0. current is: " + tileCount);
            return;
        }
        if (typeof infoType !== "undefined" && typeof infoType !== "string") {
            errorCallback("If infoType is given, it must be a string. current is: " + typeof infoType);
            return;
        }

        if (isUsageSupported()) {
            var infoObj = {};
            if (typeof tileCount === "number") {
                infoObj.tileCount = tileCount;
            }
            if (typeof infoType === "string") {
                infoObj.infoType = infoType;
            }
            cordova.exec(successCallback, errorCallback, "FioriClient", "stopLaunchpadLoadTimer", [infoObj]);
        } else {
            errorCallback("usage_not_supported", "Usage is not supported.");
        }
    };

    return {
        startLoadTimer: startLoadTimer,
        stopLoadTimer: stopLoadTimer
    };

}();

function TileObserver(classToObserve, callback) {
    // on windows we need to start/stop the observer in the webview
    var isWindows = cordova.platformId.toLowerCase() === "windows";

    if (!TileObserver.isSupported) {
        throw new Error("MutationObserver is not supported.");
    }
    if (typeof classToObserve !== 'string') {
        throw Error("TileObserver's argument must be a string.");
    }

    var _observer;
    var _callback = (typeof callback === 'function') ? callback : function tileObserverDefaultCallback() {};
    var _isRunning = false;

    this.isRunning = function isRunning() {
        return _isRunning;
    };

    this.start = function startObserver() {
        if (isWindows)
            return;
        _observer = new MutationObserver(function launchpadObserver(mutations) {
            var elementsLength = document.getElementsByClassName(classToObserve).length;
            if (elementsLength > 0) {
                window.setTimeout(function tileObserverResult() {
                    _callback('result', elementsLength);
                }, 0);
            }
        });

        var elementsLength = document.getElementsByClassName(classToObserve).length;
        if (elementsLength > 0) {
            window.setTimeout(function tileObserverResult() {
                _callback('preresult', elementsLength);
            }, 0);
        }

        var observerConfig = {
            childList: true,
            subtree: true
        };
        _observer.observe(document.body, observerConfig);
        _isRunning = true;

        return true;
    };

    this.stop = function stopObserver() {
        if (isWindows)
            return;
        if (_isRunning) {
            _isRunning = false;
            _observer.disconnect();
            _observer = undefined;
            window.setTimeout(function tileObserverResult() {
                _callback('stopped');
            }, 0);
        }
    };
}
Object.defineProperty(TileObserver, 'isSupported', {
    configurable: false,
    enumerable: true,
    get: function() {
        return (typeof MutationObserver !== 'undefined');
    }
});

function getFioriURL() {
    sap.FioriClient.getFioriURL(function(link) {
        fioriURL = link;
    });
}
               

function setToolbar() {
    if (isToolbarSupported()) {
        sap.Toolbar.addEventListener(function launchpadEventListener(eventId, itemId) {
            if (typeof itemId !== "string") {
                return;
            }

            var URLcheck = fioriURL === window.location.href.split('?')[0];
            if (cordova.platformId.toLowerCase() === "windows") {
                URLcheck = document.getElementById("webView").src.split('?')[0] === fioriURL;
            }

            if (itemId.toLowerCase() === "home" || (itemId.toLowerCase() === "refresh" && URLcheck)) {
                launchpadTimer.startLoadTimer(120000);
            }
        });
    }
}

function onDeviceReady() {
    if (typeof WinJS !== "undefined" && typeof WinJS.Application !== "undefined") {
        WinJS.Application.addEventListener("onSapLogonSuccess", getFioriURL, false);
        WinJS.Application.addEventListener("onUsageInitialized", setToolbar, false);
    } else {
        document.addEventListener("onSapLogonSuccess", getFioriURL, false);
        document.addEventListener("onUsageInitialized", setToolbar, false);
    }

    cordova.fireWindowEvent('orientationchange', {});

    cordova.exec(function launchadRunningCallback(result) {
        if (!result) {
            tileObserver.stop();
        }
    }, null, "FioriClient", "isLaunchpadTimerRunning", []);
               
}

try {
    tileObserver = new TileObserver('sapUshellTile', function tileObserverCallback(status, result) {
        if (status === 'result' || status === 'preresult') {
            launchpadTimer.stopLoadTimer(result);
            tileObserver.stop();
        }
    });
    tileObserver.start();
} catch (e) {
    console.error("[FioriClient][launchpad.js] Failed to create observer: " + e);
}

//document.addEventListener("deviceready", onDeviceReady, false);


               //https://fiorid.zaffari.com.br/sap/bc/ui5_ui5/ui2/ushell/shells/abap/FioriLaunchpad.html?sap-client=100&sap-language=PT#ZCAALLAPVEXT2-aprovarTED?Origin=ECC_BACKEND_WF%252CTS90000108&scenarioId=ZFIORI_TED_APV&/detail/ECC_BACKEND_WF/000000166350/TaskCollection(SAP__Origin='ECC_BACKEND_WF',InstanceID='000000166350')
    
               function onConfirmPush(event) {
              // jQuery.sap.require("sap.ushell.Container");
               
               this.oCrossAppNav = sap.ushell.Container.getService("CrossApplicationNavigation");
               this.oCrossAppNav.toExternal({
                                            target: { semanticObject : "ZCAALLAPVEXT2", action: "aprovarTED" },  params : {
                                            
                                            Origin : "ECC_BACKEND_WF%2CTS90000108",
                                            scenarioId : "ZFIORI_TED_APV"
                                            
                                            }
                            });
               }
               
               
var confirmAction = function(event) {
                jQuery.sap.require("sap.m.MessageBox");
               
               
               
               sap.m.MessageBox.show(
                                     event.title + "\n" + "Deseja abrir o documento?", {
                                     icon: sap.m.MessageBox.Icon.INFORMATION,
                                     title: "Novo item para aprovação",
                                     actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
                                     onClose: function(oAction) {
                                     if (oAction === sap.m.MessageBox.Action.YES) {
                                     onConfirmPush(event);
                                     }
                                     }
                                     }
                                     );
               }
  

               

this._handlePushedNotification = function (event) {
  confirmAction(event);
}
               
this._registerForPush = function () {
    sap.Push.initPush(this._handlePushedNotification.bind(this));
}
               
document.addEventListener("deviceready", this._registerForPush.bind(this), false);

               
module.exports = {
    launchpadTimer: launchpadTimer,
    tileObserver: tileObserver
};

});
