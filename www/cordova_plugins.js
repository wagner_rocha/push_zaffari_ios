cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-device/www/device.js",
        "id": "cordova-plugin-device.device",
        "pluginId": "cordova-plugin-device",
        "clobbers": [
            "device"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-inappbrowser/www/inappbrowser.js",
        "id": "kapsel-plugin-inappbrowser.inappbrowser",
        "pluginId": "kapsel-plugin-inappbrowser",
        "clobbers": [
            "cordova.InAppBrowser.open",
            "window.open"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-i18n/www/i18n.js",
        "id": "kapsel-plugin-i18n.i18n",
        "pluginId": "kapsel-plugin-i18n"
    },
    {
        "file": "plugins/kapsel-plugin-authproxy/www/authproxy.js",
        "id": "kapsel-plugin-authproxy.AuthProxy",
        "pluginId": "kapsel-plugin-authproxy",
        "clobbers": [
            "sap.AuthProxy"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-logon/www/common/modules/MAFLogonCorePlugin.js",
        "id": "kapsel-plugin-logon.LogonCore",
        "pluginId": "kapsel-plugin-logon",
        "clobbers": [
            "sap.logon.Core"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-logon/www/common/modules/LogonCoreLocalStorage.js",
        "id": "kapsel-plugin-logon.LogonLocalStorage",
        "pluginId": "kapsel-plugin-logon",
        "clobbers": [
            "sap.logon.CoreLocalStorage"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-logon/www/common/modules/Utils.js",
        "id": "kapsel-plugin-logon.LogonUtils",
        "pluginId": "kapsel-plugin-logon",
        "clobbers": [
            "sap.logon.Utils"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-logon/www/common/modules/StaticScreens.js",
        "id": "kapsel-plugin-logon.LogonStaticScreens",
        "pluginId": "kapsel-plugin-logon",
        "clobbers": [
            "sap.logon.StaticScreens"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-logon/www/common/modules/DynamicScreens.js",
        "id": "kapsel-plugin-logon.LogonDynamicScreens",
        "pluginId": "kapsel-plugin-logon",
        "clobbers": [
            "sap.logon.DynamicScreens"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-logon/www/common/modules/LogonController.js",
        "id": "kapsel-plugin-logon.Logon",
        "pluginId": "kapsel-plugin-logon",
        "clobbers": [
            "sap.Logon"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-logon/www/common/modules/LogonJsView.js",
        "id": "kapsel-plugin-logon.LogonJsView",
        "pluginId": "kapsel-plugin-logon",
        "clobbers": [
            "sap.logon.LogonJsView",
            "sap.logon.IabUi"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-logger/www/logger.js",
        "id": "kapsel-plugin-logger.Logging",
        "pluginId": "kapsel-plugin-logger",
        "clobbers": [
            "sap.Logger"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-apppreferences/www/apppreferences.js",
        "id": "kapsel-plugin-apppreferences.AppPreferences",
        "pluginId": "kapsel-plugin-apppreferences",
        "clobbers": [
            "sap.AppPreferences"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-settings/www/settings.js",
        "id": "kapsel-plugin-settings.Settings",
        "pluginId": "kapsel-plugin-settings",
        "clobbers": [
            "sap.Settings"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-settings/www/appsettings.js",
        "id": "kapsel-plugin-settings.AppSettings",
        "pluginId": "kapsel-plugin-settings",
        "merges": [
            "sap.Settings"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-online/www/online.js",
        "id": "kapsel-plugin-online.Online",
        "pluginId": "kapsel-plugin-online",
        "clobbers": [
            "sap.Online"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-toolbar/www/toolbar.js",
        "id": "kapsel-plugin-toolbar.toolbar",
        "pluginId": "kapsel-plugin-toolbar",
        "clobbers": [
            "window.sap.Toolbar"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-encryptedstorage/www/encryptedstorage.js",
        "id": "kapsel-plugin-encryptedstorage.Encrypted",
        "pluginId": "kapsel-plugin-encryptedstorage",
        "clobbers": [
            "sap.EncryptedStorage"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-barcodescanner/www/barcodescanner.js",
        "id": "kapsel-plugin-barcodescanner.BarcodeScanner",
        "pluginId": "kapsel-plugin-barcodescanner",
        "clobbers": [
            "cordova.plugins.barcodeScanner"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-fioriclient/www/fioriclient.js",
        "id": "kapsel-plugin-fioriclient.FioriClient",
        "pluginId": "kapsel-plugin-fioriclient",
        "clobbers": [
            "sap.FioriClient"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-fioriclient/www/launchpad.js",
        "id": "kapsel-plugin-fioriclient.FioriClient-Launchpad",
        "pluginId": "kapsel-plugin-fioriclient"
    },
    {
        "file": "plugins/cordova-plugin-dialogs/www/notification.js",
        "id": "cordova-plugin-dialogs.notification",
        "pluginId": "cordova-plugin-dialogs",
        "merges": [
            "navigator.notification"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-attachmentviewer/www/attachmentviewer.js",
        "id": "kapsel-plugin-attachmentviewer.AttachmentViewer",
        "pluginId": "kapsel-plugin-attachmentviewer",
        "clobbers": [
            "sap.AttachmentViewer"
        ]
    },
    {
        "file": "plugins/de.appplant.cordova.plugin.printer/www/printer.js",
        "id": "de.appplant.cordova.plugin.printer.Printer",
        "pluginId": "de.appplant.cordova.plugin.printer",
        "clobbers": [
            "plugin.printer",
            "cordova.plugins.printer"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/DirectoryEntry.js",
        "id": "cordova-plugin-file.DirectoryEntry",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.DirectoryEntry"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/DirectoryReader.js",
        "id": "cordova-plugin-file.DirectoryReader",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.DirectoryReader"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/Entry.js",
        "id": "cordova-plugin-file.Entry",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.Entry"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/File.js",
        "id": "cordova-plugin-file.File",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.File"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/FileEntry.js",
        "id": "cordova-plugin-file.FileEntry",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.FileEntry"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/FileError.js",
        "id": "cordova-plugin-file.FileError",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.FileError"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/FileReader.js",
        "id": "cordova-plugin-file.FileReader",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.FileReader"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/FileSystem.js",
        "id": "cordova-plugin-file.FileSystem",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.FileSystem"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/FileUploadOptions.js",
        "id": "cordova-plugin-file.FileUploadOptions",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.FileUploadOptions"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/FileUploadResult.js",
        "id": "cordova-plugin-file.FileUploadResult",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.FileUploadResult"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/FileWriter.js",
        "id": "cordova-plugin-file.FileWriter",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.FileWriter"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/Flags.js",
        "id": "cordova-plugin-file.Flags",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.Flags"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/LocalFileSystem.js",
        "id": "cordova-plugin-file.LocalFileSystem",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.LocalFileSystem"
        ],
        "merges": [
            "window"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/Metadata.js",
        "id": "cordova-plugin-file.Metadata",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.Metadata"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/ProgressEvent.js",
        "id": "cordova-plugin-file.ProgressEvent",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.ProgressEvent"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/fileSystems.js",
        "id": "cordova-plugin-file.fileSystems",
        "pluginId": "cordova-plugin-file"
    },
    {
        "file": "plugins/cordova-plugin-file/www/requestFileSystem.js",
        "id": "cordova-plugin-file.requestFileSystem",
        "pluginId": "cordova-plugin-file",
        "clobbers": [
            "window.requestFileSystem"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/resolveLocalFileSystemURI.js",
        "id": "cordova-plugin-file.resolveLocalFileSystemURI",
        "pluginId": "cordova-plugin-file",
        "merges": [
            "window"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/browser/isChrome.js",
        "id": "cordova-plugin-file.isChrome",
        "pluginId": "cordova-plugin-file",
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-file/www/ios/FileSystem.js",
        "id": "cordova-plugin-file.iosFileSystem",
        "pluginId": "cordova-plugin-file",
        "merges": [
            "FileSystem"
        ]
    },
    {
        "file": "plugins/cordova-plugin-file/www/fileSystems-roots.js",
        "id": "cordova-plugin-file.fileSystems-roots",
        "pluginId": "cordova-plugin-file",
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-file/www/fileSystemPaths.js",
        "id": "cordova-plugin-file.fileSystemPaths",
        "pluginId": "cordova-plugin-file",
        "merges": [
            "cordova"
        ],
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-screen-orientation/www/screenorientation.js",
        "id": "cordova-plugin-screen-orientation.screenorientation",
        "pluginId": "cordova-plugin-screen-orientation",
        "clobbers": [
            "cordova.plugins.screenorientation"
        ]
    },
    {
        "file": "plugins/cordova-plugin-screen-orientation/www/screenorientation.ios.js",
        "id": "cordova-plugin-screen-orientation.screenorientation.ios",
        "pluginId": "cordova-plugin-screen-orientation",
        "merges": [
            "cordova.plugins.screenorientation"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-voicerecording/www/recording.js",
        "id": "kapsel-plugin-voicerecording.VoiceRecording",
        "pluginId": "kapsel-plugin-voicerecording",
        "clobbers": [
            "sap.VoiceRecording"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-voicerecording/www/audiorecorder.js",
        "id": "kapsel-plugin-voicerecording.VoiceRecording-AudioRecorder",
        "pluginId": "kapsel-plugin-voicerecording"
    },
    {
        "file": "plugins/kapsel-plugin-voicerecording/www/screen.js",
        "id": "kapsel-plugin-voicerecording.VoiceRecording-AudioScreen",
        "pluginId": "kapsel-plugin-voicerecording"
    },
    {
        "file": "plugins/kapsel-plugin-voicerecording/www/recordingutils.js",
        "id": "kapsel-plugin-voicerecording.VoiceRecording-Utils",
        "pluginId": "kapsel-plugin-voicerecording"
    },
    {
        "file": "plugins/kapsel-plugin-usage/www/usage.js",
        "id": "kapsel-plugin-usage.Usage",
        "pluginId": "kapsel-plugin-usage",
        "clobbers": [
            "sap.Usage"
        ]
    },
    {
        "file": "plugins/cordova-plugin-camera/www/CameraConstants.js",
        "id": "cordova-plugin-camera.Camera",
        "pluginId": "cordova-plugin-camera",
        "clobbers": [
            "Camera"
        ]
    },
    {
        "file": "plugins/cordova-plugin-camera/www/CameraPopoverOptions.js",
        "id": "cordova-plugin-camera.CameraPopoverOptions",
        "pluginId": "cordova-plugin-camera",
        "clobbers": [
            "CameraPopoverOptions"
        ]
    },
    {
        "file": "plugins/cordova-plugin-camera/www/Camera.js",
        "id": "cordova-plugin-camera.camera",
        "pluginId": "cordova-plugin-camera",
        "clobbers": [
            "navigator.camera"
        ]
    },
    {
        "file": "plugins/cordova-plugin-camera/www/ios/CameraPopoverHandle.js",
        "id": "cordova-plugin-camera.CameraPopoverHandle",
        "pluginId": "cordova-plugin-camera",
        "clobbers": [
            "CameraPopoverHandle"
        ]
    },
    {
        "file": "plugins/cordova-plugin-geolocation/www/Coordinates.js",
        "id": "cordova-plugin-geolocation.Coordinates",
        "pluginId": "cordova-plugin-geolocation",
        "clobbers": [
            "Coordinates"
        ]
    },
    {
        "file": "plugins/cordova-plugin-geolocation/www/PositionError.js",
        "id": "cordova-plugin-geolocation.PositionError",
        "pluginId": "cordova-plugin-geolocation",
        "clobbers": [
            "PositionError"
        ]
    },
    {
        "file": "plugins/cordova-plugin-geolocation/www/Position.js",
        "id": "cordova-plugin-geolocation.Position",
        "pluginId": "cordova-plugin-geolocation",
        "clobbers": [
            "Position"
        ]
    },
    {
        "file": "plugins/cordova-plugin-geolocation/www/geolocation.js",
        "id": "cordova-plugin-geolocation.geolocation",
        "pluginId": "cordova-plugin-geolocation",
        "clobbers": [
            "navigator.geolocation"
        ]
    },
    {
        "file": "plugins/cordova-plugin-network-information/www/network.js",
        "id": "cordova-plugin-network-information.network",
        "pluginId": "cordova-plugin-network-information",
        "clobbers": [
            "navigator.connection",
            "navigator.network.connection"
        ]
    },
    {
        "file": "plugins/cordova-plugin-network-information/www/Connection.js",
        "id": "cordova-plugin-network-information.Connection",
        "pluginId": "cordova-plugin-network-information",
        "clobbers": [
            "Connection"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-cachemanager/www/cachemanager.js",
        "id": "kapsel-plugin-cachemanager.CacheManager",
        "pluginId": "kapsel-plugin-cachemanager",
        "clobbers": [
            "sap.CacheManager"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-calendar/www/calendar.js",
        "id": "kapsel-plugin-calendar.Calendar",
        "pluginId": "kapsel-plugin-calendar",
        "clobbers": [
            "Calendar"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-push/www/push.js",
        "id": "kapsel-plugin-push.Push",
        "pluginId": "kapsel-plugin-push",
        "clobbers": [
            "sap.Push"
        ]
    },
    {
        "file": "plugins/cordova-plugin-contacts/www/contacts.js",
        "id": "cordova-plugin-contacts.contacts",
        "pluginId": "cordova-plugin-contacts",
        "clobbers": [
            "navigator.contacts"
        ]
    },
    {
        "file": "plugins/cordova-plugin-contacts/www/Contact.js",
        "id": "cordova-plugin-contacts.Contact",
        "pluginId": "cordova-plugin-contacts",
        "clobbers": [
            "Contact"
        ]
    },
    {
        "file": "plugins/cordova-plugin-contacts/www/convertUtils.js",
        "id": "cordova-plugin-contacts.convertUtils",
        "pluginId": "cordova-plugin-contacts"
    },
    {
        "file": "plugins/cordova-plugin-contacts/www/ContactAddress.js",
        "id": "cordova-plugin-contacts.ContactAddress",
        "pluginId": "cordova-plugin-contacts",
        "clobbers": [
            "ContactAddress"
        ]
    },
    {
        "file": "plugins/cordova-plugin-contacts/www/ContactError.js",
        "id": "cordova-plugin-contacts.ContactError",
        "pluginId": "cordova-plugin-contacts",
        "clobbers": [
            "ContactError"
        ]
    },
    {
        "file": "plugins/cordova-plugin-contacts/www/ContactField.js",
        "id": "cordova-plugin-contacts.ContactField",
        "pluginId": "cordova-plugin-contacts",
        "clobbers": [
            "ContactField"
        ]
    },
    {
        "file": "plugins/cordova-plugin-contacts/www/ContactFindOptions.js",
        "id": "cordova-plugin-contacts.ContactFindOptions",
        "pluginId": "cordova-plugin-contacts",
        "clobbers": [
            "ContactFindOptions"
        ]
    },
    {
        "file": "plugins/cordova-plugin-contacts/www/ContactName.js",
        "id": "cordova-plugin-contacts.ContactName",
        "pluginId": "cordova-plugin-contacts",
        "clobbers": [
            "ContactName"
        ]
    },
    {
        "file": "plugins/cordova-plugin-contacts/www/ContactOrganization.js",
        "id": "cordova-plugin-contacts.ContactOrganization",
        "pluginId": "cordova-plugin-contacts",
        "clobbers": [
            "ContactOrganization"
        ]
    },
    {
        "file": "plugins/cordova-plugin-contacts/www/ContactFieldType.js",
        "id": "cordova-plugin-contacts.ContactFieldType",
        "pluginId": "cordova-plugin-contacts",
        "merges": [
            ""
        ]
    },
    {
        "file": "plugins/cordova-plugin-contacts/www/ios/contacts.js",
        "id": "cordova-plugin-contacts.contacts-ios",
        "pluginId": "cordova-plugin-contacts",
        "merges": [
            "navigator.contacts"
        ]
    },
    {
        "file": "plugins/cordova-plugin-contacts/www/ios/Contact.js",
        "id": "cordova-plugin-contacts.Contact-iOS",
        "pluginId": "cordova-plugin-contacts",
        "merges": [
            "Contact"
        ]
    },
    {
        "file": "plugins/cordova-plugin-statusbar/www/statusbar.js",
        "id": "cordova-plugin-statusbar.statusbar",
        "pluginId": "cordova-plugin-statusbar",
        "clobbers": [
            "window.StatusBar"
        ]
    },
    {
        "file": "plugins/cordova-plugin-customurlscheme/www/ios/LaunchMyApp.js",
        "id": "cordova-plugin-customurlscheme.LaunchMyApp",
        "pluginId": "cordova-plugin-customurlscheme",
        "clobbers": [
            "window.plugins.launchmyapp"
        ]
    },
    {
        "file": "plugins/kapsel-plugin-appupdate/www/appupdate.js",
        "id": "kapsel-plugin-appupdate.AppUpdate",
        "pluginId": "kapsel-plugin-appupdate",
        "clobbers": [
            "sap.AppUpdate"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "kapsel-plugin-corelibs": "3.13.4",
    "cordova-plugin-device": "1.1.3",
    "kapsel-plugin-inappbrowser": "1.3.1",
    "kapsel-plugin-i18n": "3.13.4",
    "kapsel-plugin-authproxy": "3.13.4",
    "kapsel-plugin-logon": "3.13.4",
    "kapsel-plugin-logger": "3.13.4",
    "kapsel-plugin-apppreferences": "3.13.4",
    "kapsel-plugin-settings": "3.13.4",
    "kapsel-plugin-online": "3.13.4",
    "kapsel-plugin-toolbar": "3.13.4",
    "kapsel-plugin-encryptedstorage": "3.13.4",
    "kapsel-plugin-barcodescanner": "3.13.4",
    "kapsel-plugin-fioriclient": "3.13.4",
    "cordova-plugin-dialogs": "1.3.0",
    "kapsel-plugin-attachmentviewer": "3.13.4",
    "de.appplant.cordova.plugin.printer": "0.7.0",
    "cordova-plugin-compat": "1.0.0",
    "cordova-plugin-file": "4.3.0",
    "cordova-plugin-screen-orientation": "1.4.2",
    "kapsel-plugin-voicerecording": "3.13.4",
    "kapsel-plugin-usage": "3.13.4",
    "cordova-plugin-whitelist": "1.2.2",
    "cordova-plugin-camera": "2.3.0",
    "cordova-plugin-geolocation": "2.2.0",
    "cordova-plugin-network-information": "1.2.1",
    "cordova-plugin-crosswalk-webview": "2.1.0",
    "kapsel-plugin-inappbrowser-xwalk": "3.13.4",
    "kapsel-plugin-cachemanager": "3.13.4",
    "kapsel-plugin-calendar": "4.4.4",
    "kapsel-plugin-cdsprovider": "3.13.4",
    "kapsel-plugin-push": "3.13.4",
    "kapsel-plugin-federationprovider": "3.13.4",
    "cordova-plugin-contacts": "2.2.0",
    "cordova-plugin-statusbar": "2.1.3",
    "cordova-plugin-privacyscreen": "0.3.1",
    "cordova-plugin-customurlscheme": "4.0.0",
    "kapsel-plugin-appupdate": "3.13.4",
    "kapsel-plugin-multidex": "3.13.4"
}
// BOTTOM OF METADATA
});